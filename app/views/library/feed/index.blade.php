<?xml version="1.0" encoding="UTF-8" ?>
<rss xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:wfw="http://wellformedweb.org/CommentAPI/"
  xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:atom="http://www.w3.org/2005/Atom"
  xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
  version="2.0">
  <channel>
    <atom:link href="{{ home_url('feed') }}" rel="self" type="application/rss+xml" />
    <title>{{ sitename() }} RSS</title>
    <description>{{ config('site.description') }} RSS</description>
    <link>{{ home_url() }}</link>
    <lastBuildDate>{{ date('r') }}</lastBuildDate>
    @if ($rss)
      @foreach ($rss as $term)
        <item>
          <title>{{ $term }}</title>
          <link>{{ permalink($term) }}</link>
          <description>
            <![CDATA[<div>{{ $term }}</div>]]>
          </description>
          <pubDate>{{ date('r') }}</pubDate>
          <guid>{{ permalink($term) }}</guid>
        </item>
      @endforeach
    @endif
  </channel>
</rss>
