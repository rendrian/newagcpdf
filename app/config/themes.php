<?php

/*
 *	Themes Configuration
 */

return [
	'active_theme'  	=> 'superseortm',
	'attachment'		=> true,
	'page'				=> ["contact", "disclaimer", "privacy",  "terms"], // list name pages

];
