<?php

return [
  "external_grabber_status" => false,
  "external_grabber_url" => "https://grabber-api.dev/",
  "external_grabber_api_key" => "dccc621a29194fda85088931f4c539f0",
  "shuffle_data" => true,
  "is_agc_pdf" => true,
  "use_sentence_finder" => true,
  "agc_pdf_download_image" => "https://lh3.googleusercontent.com/-ldP_n7g0HeM/YTHoPr8A2sI/AAAAAAAAAAc/Iky7NE5h-9seNRFUA0BUwzT7SqS3zue7wCLcBGAsYHQ/unnamed.png",
  "title_check_other_document" => "Also Check Another PDF",
  "remove_from_keyword" => ['download', 'pdf', 'doc', 'docx'],
];
