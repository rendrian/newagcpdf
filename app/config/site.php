<?php

/*
 *	Site Title & Description
 */

return [

	'title'         => 'Gallery PDF',
	'url'         => 'agcpdfzai.dev',

	'description'   => 'Your Document Search Engine',

	// if false, meta title & description will be displayed from /themes/static/meta.blade.php
	'custom_meta'	=> true,

	'index'	=>  [
		'title'			=> '{title} - {description} | {sitename}',
		'description'	=> '{title} - {description} | {sitename}',
	],

	'search' =>  [
		'title'			=> 'Download Ebook {query|ucwords} PDF - {title}',
		'description'	=> '{query|strtolower} - {count} images - {random|results|5|, |strtolower|true}',
	],
	'attachment' =>  [
		'title'			=> '{query|ucwords} | {title}',
		'description'	=> '{query|strtolower} - {query|strtolower} | {random|results|5|, |strtolower|true}',
	],
	'page' =>  [
		'title'			=> '{page_title} Page - {title}',
		'description'	=> '{page_title} Page - {title}',
	],
	'list' =>  [
		'title'			=> 'Index post with abjad : {list_title} - {title}',
		'description'	=> 'Index post with abjad : {list_title} - {title}',
	],

];
