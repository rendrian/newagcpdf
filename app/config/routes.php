<?php

/*
 *	Router Pattern Regex Configuration
 */

return [

	'(a)'   => '([a-zA-Z]+)',
	'(n)' 	=> '([0-9]+)',
	'(uri)' => '([a-zA-Z0-9\-\_]+)',
	'(l)'  	=> '([a-z]+)',
	'(u)'  	=> '([A-Z]+)',
	'(any)' => '([\W\w]+)',
	'(all)'	=> '([^\/|.]+)', //'((?!\/)(?!\.)[\s\S])', //'([a-zA-Z0-9\-\_]+)',
	'(x)'	=> '([^\/._-]+)', // route pattern for random, firstword, and firstchar
];
