<?php

/*
 *	Meta Google And Bing Verification
 */

return [

	// <meta name="google-site-verification" content="18y7278h1jg16t2bf12yu12f1r672g">
	// copy the value only : 18y7278h1jg16t2bf12yu12f1r672g
	
    'verification' => [
      		'prettylifesailing.com'   => [
      			  'google'  => 'Sg7hGMi8eqPuvrci0_ttpICWagrgcn4KyitBRf4mmrg', 
      			  'bing'    => '770DC303C085EE89B1A93D15BC0404B6',
      		],
      		'hemorroidestratamientorapido.com'   => [
      			  'google'  => '93XwusB3GvKcjBjtjB8nkCjT6hfaDIhXi_BwOsIf6S4',
      			  'bing'    => '770DC303C085EE89B1A93D15BC0404B6',
      		],
      		'domain3.com'   => [
      			  'google'  => '',
      			  'bing'    => '',
      		],
	],

];
