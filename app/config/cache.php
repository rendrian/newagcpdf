<?php

/*
 *	Cache Configuration
 */

return [

	'driver'				=> 'files',
	'dir'  					=> 'cache',
	'path'  => [
		'sub'					=> 'child',
		'views'     	=> 'views'
	],
	'disabled'      => false,

];
