<?php

/*
 *	Proxy Setup
 */

return [

    'active' => false,

    // proxy:port|username:password

    'proxy'    => [

            '123.123.123.121:8080|null:null',
            '123.123.123.122:8080|null:null',
            '123.123.123.123:8080|null:null',
            '123.123.123.124:8080|null:null',
            '123.123.123.125:8080|null:null',

    ],
];
