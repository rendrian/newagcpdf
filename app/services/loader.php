<?php

class Autoloader
{
    protected static $fileType = '.php';
    protected static $pathTop = __DIR__;
    protected static $fileIterator = null;
    protected static $filePath = null;

	public static function loader($className)
    {
          $cache_path     = include( APP_PATH . '/config/cache.php');
      		$classname 			= explode('\\', $className);
      		$class 				  = end( $classname );
      		$class_filename = $class . static::$fileType;

    			if( !is_dir( $folderPath = BASE_PATH . "/" .$cache_path['dir'] ) )
    			{
    				mkdir("$folderPath");
    				chmod("$folderPath", 0777);
    			}

      		$cache_file 		= BASE_PATH . "/{$cache_path['dir']}/".md5(BASE_PATH)."_classpaths.cache";
      		$path_cache 		= (file_exists($cache_file)) ? unserialize(file_get_contents($cache_file)) : array();
      		if (!is_array($path_cache)) {
            $path_cache = array();
          }

      		if (array_key_exists($class, $path_cache)) {
      			if (file_exists($path_cache[$class])) {
                require_once $path_cache[$class];
            }
      		} else {
      			$directory = new RecursiveDirectoryIterator(static::$pathTop, RecursiveDirectoryIterator::SKIP_DOTS);
      			if (is_null(static::$fileIterator)) {
      				static::$fileIterator = new RecursiveIteratorIterator($directory, RecursiveIteratorIterator::LEAVES_ONLY);
      			}
      			foreach(new RecursiveIteratorIterator($directory) as $file) {
      				if ( $file->getFilename() == $class_filename) {
      					$full_path = $file->getRealPath();
      					$path_cache[$class] = $full_path;
      					require_once $full_path;
      					break;
      				}
      			}

      		}
      		$serialized_paths = serialize($path_cache);
      		if ($serialized_paths != $path_cache) {
            file_put_contents($cache_file, $serialized_paths);
          }

    }

    public static function setFiles( $filePath )
    {
        if( $files = glob( rtrim($filePath, '/') . '/*.php') )
        {
            foreach ( $files as $file ) {
                if ( is_readable( $file ) ) {
                    include_once $file;
                }
            }
        }
    }

    public static function setType($fileType)
    {
        static::$fileType = $fileType;
    }

    public static function setPath($path)
    {
        static::$pathTop = $path;
    }

    public static function vendor()
    {
        if( !file_exists( BASE_PATH . '/vendor/autoload.php' ) )
        {
        	 die("<h2 style='color: rgb(190, 50, 50);'>Please run \"composer install\" first!</h2>");
        }
		include BASE_PATH . '/vendor/autoload.php';
    }

    public static function register()
    {
        spl_autoload_register('self::vendor');
        spl_autoload_register('self::loader');
    		$ErrorHandler = new ErrorHandler();
    		$ErrorHandler->register();
    }

}

// SET AUTOLOADER
Autoloader::setPath( APP_PATH );
Autoloader::setFiles( APP_PATH . '/library/helper');
Autoloader::register();
