<script>
  $(window).scroll(function() {
    if ($(this).scrollTop() > 200) {
      $('#back-to-top').fadeIn();
    } else {
      $('#back-to-top').fadeOut();
    }
  });
  $('#back-to-top').hide().click(function() {
    $('html, body').animate({
      scrollTop: 0
    }, 1000);
    return false;
  });



  jQuery(document).ready(function() {
    jQuery("#comments p").find("a").replaceWith("<mark>Spam Detected!</mark>");
  });



  $(".Header h1").each(function() {
    var t = $(this).text().split(" ");
    t.length < 2 || (t[1] = '<span class="secondWord">' + t[1] + "</span>", $(this).html(t.join(" ")))
  });




  $(function() {
    if ($('#Label1').length) {
      var el = $('#Label1');
      var stickyTop = $('#Label1').offset().top;
      var stickyHeight = $('#Label1').height();
      $(window).scroll(function() {
        var limit = $('#footer-wrapper').offset().top - stickyHeight - 20;
        var windowTop = $(window).scrollTop();
        if (stickyTop < windowTop) {
          el.css({
            position: 'fixed',
            top: 80
          });
        } else {
          el.css('position', 'static');
        }
        if (limit < windowTop) {
          var diff = limit - windowTop;
          el.css({
            top: diff
          });
        }
      });
    }
  });




  $(window).scroll(function(e) {
    if ($(window).scrollTop() > 0) {
      $(".header").addClass("fixed-header");
    } else {
      $(".header").removeClass("fixed-header");
    }
  });

  $(document).ready(function(e) {

    if ($(window).scrollTop() > 0) {
      $(".header").addClass("fixed-header");
    }

    $(".search-box .search-icon").click(function(e) {
      $(this).siblings(".search-input").slideToggle();
      if ($(window).width() < 1081) {
        $(".navigation").css("left", "-201px");
        $(".toggle-mobile-btn").removeClass("open");
      }
    });

    $(".toggle-mobile-btn").click(function(e) {
      if ($(this).hasClass("open")) {
        $(this).removeClass("open");
        $(".navigation").css("left", "-201px");
      } else {
        $(this).addClass("open");
        $(".search-input").fadeOut();
        $(".navigation").css("left", "0");
      }
    });
  });

  $(window).on("resize load", function() {
    if ($(window).width() > 1080) {
      $(".navigation").removeAttr("style");
      $(".toggle-mobile-btn").removeClass("open");
    }
  });




  // Loadmore
  ! function(i) {
    function e() {
      t || (t = !0, n ? (o.find("a").hide(), o.find("img").show(), i.ajax(n, {
        dataType: "html"
      }).done(function(e) {
        e = i("<div></div>").append(e.replace(l, ""));
        var a = e.find("a.blog-pager-older-link");
        a ? n = a.attr("href") : (n = "", o.hide()), e = e.find(d).children(), i(d).append(e), window._gaq &&
          window._gaq.push(["_trackPageview", n]), window.gapi && window.gapi.plusone && window.gapi.plusone
          .go && window.gapi.plusone.go(), window.disqus_shortname && i.getScript("http://" + window
            .disqus_shortname + ".disqus.com/blogger_index.js"), window.FB && window.FB.XFBML && window.FB.XFBML
          .parse && window.FB.XFBML.parse(), o.find("img").hide(), o.find("a").show(), t = !1
      })) : o.hide())
    }

    function a() {
      var i = Math.max(r.height(), s.height(), document.documentElement.clientHeight),
        a = r.scrollTop() + r.height();
      0 > i - a && e()
    }
    var n = "",
      o = null,
      d = "div.blog-posts",
      t = !1,
      r = i(window),
      s = i(document),
      l = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;

  }(jQuery);
</script>
