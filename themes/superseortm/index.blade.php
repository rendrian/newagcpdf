@extends('layout')

@section('content')

  @php
  // shuffle($results);
  @endphp
  @foreach ($results['images'] as $i => $item)
    @if ($i !== 0)
      <div class="date-outer">
        <div class="date-posts">

          <div class="post-outer">
            <div class="post">
              <a name="3568435638400314300"></a>
              <div class="post-body entry-content" id="post-body-3568435638400314300" itemprop="articleBody">
                <a class="thumb" href="{{ permalink($item['title']) }}">
                  <img alt="Template Blog Paling SEO Friendly: Super SEO Blogger Template" class="lazy"
                    height="90" src="{{ filterImage($item['thumbnail']) }}" width="170">
                </a>
                <h2 class="post-title entry-title" itemprop="name">
                  <a href="{{ permalink($item['title']) }}">{{ ucwords($item['title']) }}</a>
                </h2>
                Download PDF / ebook dari {{ $item['title'] }}…
                <div style="clear: both;"></div>
              </div>
            </div>
          </div>

        </div>
      </div>
    @endif

  @endforeach



@endsection
