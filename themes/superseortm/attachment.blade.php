@extends('layout')

@section('ads')
  {!! ads('responsive') !!}
@endsection

@section('h1')
  {{ $subsuqery }}
@endsection

@section('breadcumbs')
  <a href='https://superseortm.blogspot.com/search/label/Blogging?&max-results=8' itemprop='item' rel='nofollow'
    title='Blogging'>
    <span itemprop='name'>{{ spin('{Download PDF | Download Ebook }') }}{{ $query }}</span>
  </a>
@endsection

@section('content')
  <div class="date-outer">
    <div class="date-posts">
      <div class='post-outer'>
        <div class='post'>

          {{-- <a name='-xxx'>dsfds</a> --}}
          <h1 class='post-title entry-title' itemprop='name'>{{ $query }}</h1>
          <div class='post-header'>

          </div>
          <div class='post-body entry-content' id='post-body--xxx' itemprop='articleBody'>

            <div id='ads2-xxx'>

              <div class="separator" style="clear: both; text-align: center;">
                <a href="{{ permalink($query, $results[0]['title']) }}" style="margin-left: 1em; margin-right: 1em;">
                  <img alt="{{ $results[0]['title'] }}" border="0" data-original-height="941" data-original-width="1721"
                    src="{{ $results[0]['thumbnail'] }}" title="{{ $results[0]['title'] }}" />
                </a>
              </div>


              <div>
                @includeIf('spin.attach-1')

              </div>

              <div class="pdf-download-area">
                @if (!empty($pdfs))
                  <table class="pdf-table-area">
                    <thead>
                      <tr>
                        <td>#</td>
                        <td>PDF Title</td>
                      </tr>
                    </thead>

                    <tbody>
                      @foreach ($pdfs as $key => $pdf)
                        <tr>
                          <td>
                            {{ $key + 1 }}
                          </td>
                          <td>
                            <a target="_blank" href="{{ $pdf['pdflink'] }}">{{ $pdf['title'] }}</a>
                          </td>
                        </tr>


                      @endforeach

                    </tbody>
                  </table>
                @endif



              </div>

              <div class="gallery-container">
                <div class="gallery">

                  @includeIf('partial.gallery')
                </div>

              </div>




            </div>
            <div>
              <br />
            </div>
          </div>

          <div class='iklan-dalam'></div>
          <div style='clear: both;'></div>
        </div>

      </div>
    </div>
  </div>








  {{-- <div class="download-pdf">
    @if (count($pdfs) > 0)
      <a href="{{ attachment_url($query, $results[0]['title']) }}"
        title="Download Buttom For  {{ $query }} {{ spin('{Ebook|PDF}') }}">
        <img src="{{ config('extra.agc_pdf_download_image') }}"
          alt="Download {{ $query }} {{ spin('{Ebook|PDF}') }}">
      </a>

    @endif
  </div> --}}








@endsection
