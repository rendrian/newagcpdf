@php
$sentences = collect($sentences);
@endphp
@extends('layout')



@section('breadcumbs')
  <a href='{{ currentUrl() }}' itemprop='item' rel='nofollow' title='Blogging'>
    <span itemprop='name'>{{ spin('{Download PDF | Download Ebook }') }}{{ $query }}</span>
  </a>
@endsection
@section('content')


  <div class="date-outer">
    <div class="date-posts">
      <div class='post-outer'>
        <div class='post'>

          {{-- <a name='-xxx'>dsfds</a> --}}
          <h1 class='post-title entry-title' itemprop='name'>{{ $query }}</h1>
          <div class='post-header'>

          </div>
          <div class='post-body entry-content' id='post-body--xxx' itemprop='articleBody'>

            <div id='ads2-xxx'>

              <div class="separator" style="clear: both; text-align: center;">
                <a href="{{ attachment_url($query, $results[0]['title']) }}"
                  style="margin-left: 1em; margin-right: 1em;">
                  <img alt="{{ $results[0]['title'] }}" border="0" data-original-height="941" data-original-width="1721"
                    src="{{ $results[0]['thumbnail'] }}" title="{{ $results[0]['title'] }}"
                    alt="Permalink  to {{ $results[0]['title'] }}" />
                </a>
              </div>
              <br />
              <p style="text-align: justify;">
                {!! $sentences->shuffle()->take(5)->implode('<br/>') !!}
              </p>

              <div>
                @includeIf('spin.search-1')

              </div>
              <div class="download-pdf">
                @if (count($pdfs) > 0)
                  <a href="{{ attachment_url($query, $results[0]['title']) }}"
                    title="Download Button For  {{ $query }} {{ spin('{Ebook|PDF}') }}">
                    <img src="{{ config('extra.agc_pdf_download_image') }}"
                      alt="Download {{ $query }} {{ spin('{Ebook|PDF}') }}">
                  </a>

                @else
                  <div class="gallery-container">
                    <div class="gallery">

                      @includeIf('partial.gallery')
                    </div>

                  </div>

                @endif
              </div>



              <div class="another-document">
                <h3>{{ config('extra.title_check_other_document') }}</h3>
              </div>

              @php
                shuffle($results);
              @endphp
              @foreach ($results as $i => $item)
                @if ($i !== 0)
                  <div class="date-outer">
                    <div class="date-posts">

                      <div class="post-outer">
                        <div class="post">

                          <div class="post-body entry-content" id="post-body-3568435638400314300" itemprop="articleBody">
                            <a class="thumb" href="{{ permalink($item['title']) }}">
                              <img alt="Download Ebook {{ $item['title'] }}" class="lazy" height="90"
                                src="{{ filterImage($item['thumbnail']) }}" width="170">
                            </a>
                            <h2 class="post-title entry-title" itemprop="name">
                              <a href="{{ permalink($item['title']) }}">{{ ucwords($item['title']) }}</a>
                            </h2>
                            Download PDF / ebook dari {{ $item['title'] }}…
                            <div style="clear: both;"></div>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                @endif

              @endforeach



            </div>
            <div>
              <br />
            </div>
          </div>

          <div class='iklan-dalam'></div>
          <div style='clear: both;'></div>
        </div>

      </div>
    </div>
  </div>



@endsection
