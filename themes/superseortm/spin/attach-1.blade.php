<p>
  {{ spin('{Are you|Do you}') }} {{ spin('{looking for|finding}') }} something related to
  <strong>{{ $query }}</strong> ? 
</p>
