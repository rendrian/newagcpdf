@extends('layout')

@section('content')

<h1>{{ $list_title }}</h1>

            <ul class="c listhome">
			@foreach( $random_terms as $term )
               <li><a href="{{ permalink($term) }}">{{ $term }}</a></li>
			@endforeach
            </ul>
@endsection