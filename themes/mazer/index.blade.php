@extends('layout')

@section('content')


  <section class="hero is-small has-text-centered">
    <div class="hero-body">

      <h1 class="title has-text-link-dark">Welcome To : {{ sitename() }} - {{ config('site.description') }}</h1>
      <hr>
      @foreach ($random_terms as $term)
        <a class="button funbtn is-medium " href="{{ permalink($term) }}"
          title="{{ ucwords($term) }}">{{ ucwords($term) }}</a>
      @endforeach


    </div>


    {{-- <div class="row">
    @foreach ($random_terms as $term)
      <div class="col-12 col-md-3 mt-1">
        <a class="btn btn-sm btn-block btn-primary" href="{{ permalink($term) }}"
          title="{{ ucwords($term) }}">{{ ucwords($term) }}</a>
      </div>
    @endforeach

  </div> --}}


  @endsection
